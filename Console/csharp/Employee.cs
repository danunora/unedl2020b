using System;

namespace Empleados{

class Employee {
   public int salary = 0;

   public Employee(int annualSalary){
      System.Console.WriteLine("Constructor uno");
      salary = annualSalary;
      System.Console.WriteLine("Salary: {0}",salary);
   }

   public Employee(int weeklySalary, int workedWeeks) {
      System.Console.WriteLine("Constructor dos");
      salary = weeklySalary * workedWeeks;
      System.Console.WriteLine("Salary: {0}",salary);
   }

   ~Employee(){
      System.Console.WriteLine("Llamando al destructor");
   }

}

   class Test {
	static void Main(String [] args){
	   Employee emp1 = new Employee(200);
	   Employee emp2 = new Employee(50, 10);
	}
    }
}
