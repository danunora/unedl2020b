using System;

/*
* Parent class Vehiculo
*/
public class Vehiculo {

   public String color;
   public String placas;

   public void Encender(){
       System.Console.WriteLine("Encendiendo el vehiculo");
   }

   public void Encender(String vehiculo){
       System.Console.WriteLine("Encendiendo el {0}",vehiculo);
   }
   
}

/*
* Automovil inherits from Vehiculo
*/
public class Automovil : Vehiculo {
   public void Apagar(){
       System.Console.WriteLine("Apagando el automovil");
   }
}

/*
* Main class for testing
*/
public class Test{
   static void Main(String [] args){

       Vehiculo vh1 = new Vehiculo();
       vh1.Encender();
//       vh1.Apagar();

       Automovil au1 = new Automovil();
       au1.Encender("automovil");
       au1.Apagar();

   }
}
