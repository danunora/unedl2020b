using System;
using System.IO;

namespace Practica{

    public class ArchivosOperation{

		/*
         * Crear un archivo dado
        */
		public void CrearArchivo(String myArchivo){
			if (!File.Exists(myArchivo)){
				try{
					using(StreamWriter sw = File.CreateText(myArchivo));
				}
				catch (UnauthorizedAccessException e){
					Console.WriteLine ("El archivo {0} no pudo ser creado",
					                   myArchivo);
					Console.WriteLine (e.ToString());
					System.Environment.Exit(-1);
				}
				catch (Exception e){
					Console.WriteLine ("Exception {0}", e.ToString());
				}
			}
			else {
				Console.WriteLine ("El archivo {0} ya existe",myArchivo);
			}
		}

        /*
         * Escribir a un archivo dado
        */
        public void EscribeArchivo(String myArchivo, String [] myStrings){
			try{
                using (StreamWriter sw = new StreamWriter(myArchivo)) {
                    foreach (String s in myStrings){
                       sw.WriteLine(s); 
                    }
                }
			}
			catch (FileNotFoundException e) {
				Console.WriteLine ("El archivo {0} no existe",myArchivo);
				Console.WriteLine (e.ToString());
			}
			catch (Exception e) {
				Console.WriteLine ("Exception {0}", e.ToString());
			}           
        }

        /*
         * Leer del archivo dado
        */
        public void LeerArchivo(String myArchivo){
            String line = "";
			try{
                using (StreamReader sr = new StreamReader(myArchivo)) {
                    while ((line = sr.ReadLine()) != null){
                        Console.WriteLine(line);
                    }
			    }
            }
			catch (FileNotFoundException e) {
				Console.WriteLine ("El archivo {0} no existe", myArchivo);
				Console.WriteLine (e.ToString());
			}
			catch (Exception e) {
				Console.WriteLine ("Exception {0}", e.ToString());
			}           
		}
    }

}
