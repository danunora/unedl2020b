using System;

namespace CalculatorApplication{

    class NumberManipulator {

       /*
        * Constructor
       */
       public NumberManipulator() {
          System.Console.WriteLine("Calling contructor");
       }

        /* 
         * Metodo para obtener el numero mayor
        */
        public int FindMax(int num1, int num2) {
            int result;
            if (num1 > num2) {
               result = num1;
            }
            else {
               result = num2;
            }
            return result;
        }

        /*
         * Metodo para obtener el numero menor
        */
        public int FindMin(int num1, int num2) {
            if (num1 < num2) {
               return num1;
            }
            else {
               return num2;
            }
        }
    }

    class Test {
        static void Main(string[] args) {
            int a = 100;
            int b = 200;
            int c = 150;
            int ret = 0;
            NumberManipulator n1 = new NumberManipulator();
            NumberManipulator n2 = new NumberManipulator();

            ret = n1.FindMax(a, b);
            Console.WriteLine("Max value is : {0}", ret);

            Console.WriteLine("Min value is : {0}", n2.FindMin(a,c));
            Console.ReadLine(); 
        }
    }
}
