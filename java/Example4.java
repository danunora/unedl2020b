import java.util.Scanner;

public class Example4 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Int: ");
        int i = scan.nextInt();
        System.out.println("Doble: ");
        Double d = scan.nextDouble();
        scan.nextLine();
        System.out.println("String: ");
        String s = scan.nextLine();

        // Write your code here.

        System.out.println("String: " + s);
        System.out.println("Double: " + d);
        System.out.println("Int: " + i);
    }
}
