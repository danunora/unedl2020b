import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Example1 {

    private static final Scanner scanner = new Scanner(System.in);

    public static void Metodo1(int N){
            if (N > 0 && N <= 100) {
               if (N % 2 == 1){
                    System.out.println("Weird");
               }
               else {
                    if (N >= 2 && N <= 5){
                        System.out.println("Not Weird");
                    }
                    if (N >= 6 && N <= 20){
                        System.out.println("Weird");
                    }
                    if (N >= 20)
                        System.out.println("Not Weird");
               }
            }
    }

    public static void Metodo2(int N){
        System.out.println(((N % 2 == 1) || (N >= 6 && N <= 20)) ? "2Weird" : "2Not Weird");
    }

    public static void main(String[] args) {
        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (N = 0; N <= 10; N++){
            System.out.println("N: "+N);
            Metodo1(N);
            Metodo2(N);
        }

        scanner.close();
    }
}
